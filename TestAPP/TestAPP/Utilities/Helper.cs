﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TestAPP
{
    public class Helper
    {
        private static Helper _instance = null;
        public static Helper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Helper();
            }
            return _instance;
        }
        public Helper()
        {

        }
        public bool IsNetworkConnected()
        {
            try
            {
                var networkConnection = DependencyService.Get<INetworkConnection>();
                networkConnection.CheckNetworkConnection();
                return networkConnection.IsNetworkConnected;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public double SizeHeight(double controlHeight)
        {

            controlHeight = (controlHeight / 667) * DeviceInfo.deviceHeight;

            return controlHeight;
        }

        public double SizeWidth(double controlWidth)
        {

            controlWidth = (controlWidth / 375) * DeviceInfo.deviceWidth;

            return controlWidth;
        }

        public Thickness CommonPadding(double left, double top, double right, double bottom)
        {
            return new Thickness(SizeWidth(left), SizeHeight(top), SizeWidth(right), SizeHeight(bottom));
        }
    }
}