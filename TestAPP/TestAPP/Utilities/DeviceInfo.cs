﻿using System;
namespace TestAPP
{
    public static class DeviceInfo
    {
        public static double deviceWidth { get; set; }
        public static double deviceHeight { get; set; }
        public static double statusBarHeight { get; set; }
        public static double navigationBarHeight { get; set; }
        public static string deviceFamily { get; set; }
        public static string currentVersion { get; set; }
        public static string deviceName { get; set; }
    }
}
