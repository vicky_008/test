﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestAPP.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartPage : ContentPage
	{
        CustomTitleStack Titlebar;

        public StartPage ()
		{
			InitializeComponent ();
            InitUI();
        }
        private void InitUI()
        {
            Titlebar = new CustomTitleStack(this, "Test App");
            Titlebar.LeftImage.Source = "settings.png";
            Titlebar.RightImage.Source = "calendar.png";
            Titlebar.LeftImageClicked += OnSettingsClicked;
            Titlebar.RightImageClicked += OnCalendarClicked;
            Titlebar.SubTitleLabel.Text = "tab1";

            TabView tabs = new TabView(new List<string>() { "tab1", "tab2", "tab3" });
            tabs.VerticalOptions = LayoutOptions.EndAndExpand;
            tabs.TabClicked += OnTabsClicked;

            this.Content = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { Titlebar,tabs }
            };
        }

        private void OnCalendarClicked(object sender, EventArgs e)
        {
            //calendar
        }

        private void OnSettingsClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SettingsPage());
        }

        private void OnTabsClicked(object sender, string SelectedTabTitle)
        {
            Titlebar.SubTitleLabel.Text = SelectedTabTitle;
            //Switch Views logic
        }
    }
}