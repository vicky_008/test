﻿using System;
namespace TestAPP
{
    public interface INetworkConnection
    {
        //Network Reachablity
        bool IsNetworkConnected { get; }
        void CheckNetworkConnection();
    }
}
