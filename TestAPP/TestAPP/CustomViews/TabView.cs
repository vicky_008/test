﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace TestAPP
{
    public class TabView : StackLayout
    {
        double tabWidth, tabHeight;
        List<StackLayout> tabsList;
        public event EventHandler<string> TabClicked = delegate { };
        public TabView(List<string> tabData)
        {
            BackgroundColor = Color.Transparent;
            int count = tabData.Count;
            tabWidth = DeviceInfo.deviceWidth / count;
            tabHeight = Helper.GetInstance().SizeHeight(45);

            tabsList = new List<StackLayout>();

            for (int i = 0; i < count; i++)
            {
                tabsList.Add(GenTabs(tabData[i], i));
            }

            Spacing = 0;
            Orientation = StackOrientation.Horizontal;
            WidthRequest = DeviceInfo.deviceWidth - count;
            HeightRequest = tabHeight;
            HorizontalOptions = LayoutOptions.Fill;
            VerticalOptions = LayoutOptions.Start;

            for (int i = 0; i < count; i++)
            {
                Children.Add(tabsList[i]);
                if (i < count - 1)
                    Children.Add(TabSeparator());
            }
        }
        public StackLayout GenTabs(string tabTitle, int index)
        {
            var tabStack = new StackLayout
            {
                WidthRequest = tabWidth,
                HeightRequest = tabHeight,
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.Transparent,
                Spacing = 0,
                ClassId = tabTitle
            };

            var titleStack = new StackLayout
            {
                WidthRequest = tabWidth,
                HeightRequest = tabHeight - Helper.GetInstance().SizeHeight(5),
                BackgroundColor = Color.Transparent
            };
            var titleLabel = new Label
            {
                Text = tabTitle,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                StyleId = "title",
                ClassId = tabTitle
            };
            var tabBorderLabel = new Label
            {
                BackgroundColor = Color.Black,
                HeightRequest = Helper.GetInstance().SizeHeight(1),
                WidthRequest = tabWidth,
            };

            if (index == 0)
                tabStack.BackgroundColor =Color.LightBlue;

            var tabTapGesture = new TapGestureRecognizer();
            tabTapGesture.Tapped += (sender, e) =>
            {
                var parentStack = sender as StackLayout;
                tabsList.ForEach((tab) =>
                {
                    if (tab.ClassId != parentStack.ClassId)
                        tab.BackgroundColor = Color.White;
                });

                parentStack.BackgroundColor = Color.LightBlue;

                TabClicked(sender, parentStack.ClassId);
            };

            tabStack.Children.Add(tabBorderLabel);
            tabStack.Children.Add(titleLabel);
            tabStack.GestureRecognizers.Add(tabTapGesture);

            return tabStack;
        }

        public Label TabSeparator()
        {
            var sep = new Label
            {
                HeightRequest = tabHeight - Helper.GetInstance().SizeHeight(20),
                WidthRequest = Helper.GetInstance().SizeWidth(1),
                BackgroundColor =Color.Black,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            return sep;
        }

        public double TabViewHeight
        {
            get
            {
                return DeviceInfo.navigationBarHeight + DeviceInfo.statusBarHeight + tabHeight;
            }

        }
    }
}

