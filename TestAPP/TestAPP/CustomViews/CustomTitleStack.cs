﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestAPP
{
    public class CustomTitleStack : StackLayout
    {
        Label titleLabel, lineLabel, subTitleLabel;
        Image rightImage, leftImage;
        Grid gridTitle;
        bool isClickBack = false;

        public event EventHandler<EventArgs> LeftImageClicked = delegate { };
        public event EventHandler<EventArgs> RightImageClicked = delegate { };

        public double titleHeight
        {
            get
            {
                return DeviceInfo.navigationBarHeight - 1;
            }
        }

        public CustomTitleStack(Page page, String title, string subTitle = "")
        {
            NavigationPage.SetHasBackButton(page, false);
            NavigationPage.SetHasNavigationBar(page, false);
            this.BackgroundColor = Color.Transparent;
            this.VerticalOptions = LayoutOptions.StartAndExpand;
            this.Orientation = StackOrientation.Vertical;
            this.Spacing = 0;


            var statusBarStack = new StackLayout
            {
                HeightRequest = DeviceInfo.statusBarHeight,
                BackgroundColor = Color.Transparent
            };

            var titleBarStack = new StackLayout
            {
                Spacing = 0,
                Orientation = StackOrientation.Horizontal,
                HeightRequest = titleHeight,
                BackgroundColor = Color.Transparent
            };
            if (subTitle != "")
            {
                titleBarStack.HeightRequest = titleHeight + Helper.GetInstance().SizeHeight(10);
            }
            var titleLabelStack = new StackLayout
            {
                Spacing = 0,
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            titleLabel = new Label
            {
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = title,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.Black,
                LineBreakMode = LineBreakMode.TailTruncation,
                WidthRequest = (DeviceInfo.deviceWidth / 10) * 7
            };
#if __ANDROID__
            titleLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
#endif
            subTitleLabel = new Label
            {
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start,
                Text = subTitle,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.Black,
                LineBreakMode = LineBreakMode.TailTruncation
            };

            titleLabelStack.Children.Add(titleLabel);
            titleLabelStack.Children.Add(subTitleLabel);

            lineLabel = new Label
            {
                BackgroundColor = Color.Black,
                HeightRequest = 1

            };

            var rightImageStack = new StackLayout
            {
                Spacing = 0,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center
            };

            rightImage = new Image
            {
                HeightRequest = Helper.GetInstance().SizeWidth(22),
                WidthRequest = Helper.GetInstance().SizeWidth(22),
                HorizontalOptions = LayoutOptions.Start,
            };


            rightImageStack.Children.Add(rightImage);

            var rightImageTapGestureRecognizer = new TapGestureRecognizer();
            rightImageTapGestureRecognizer.Tapped += (s, e) =>
            {
                RightImageClicked(s, e);
            };
            rightImage.GestureRecognizers.Add(rightImageTapGestureRecognizer);


            var leftImageStack = new StackLayout
            {
                Spacing = 0,
                VerticalOptions = LayoutOptions.Center
            };

            leftImage = new Image
            {
                HeightRequest = Helper.GetInstance().SizeWidth(22),
                WidthRequest = Helper.GetInstance().SizeWidth(22),
                HorizontalOptions = LayoutOptions.Start
            };

            leftImageStack.Children.Add(leftImage);

            var leftImageTapGestureRecognizer = new TapGestureRecognizer();
            leftImageTapGestureRecognizer.Tapped += (s, e) =>
            {
                LeftImageClicked(s, e);
            };
            leftImage.GestureRecognizers.Add(leftImageTapGestureRecognizer);

            if (DeviceInfo.deviceFamily == "iPad")
            {
                rightImage.HeightRequest = 22;
                rightImage.WidthRequest = 22;
                rightImageStack.Padding = new Thickness(0, 10, 8, 0);
            }

            gridTitle = new Grid
            {
                BackgroundColor = Color.Transparent,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                WidthRequest = DeviceInfo.deviceWidth,

                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength((DeviceInfo.deviceWidth)  , GridUnitType.Absolute) }
                }
            };

            gridTitle.Children.Add(titleLabelStack, 0, 0);
            gridTitle.Children.Add(leftImageStack, 0, 0);
            gridTitle.Children.Add(rightImageStack, 0, 0);

            titleBarStack.Children.Add(gridTitle);

            this.Children.Add(statusBarStack);
            this.Children.Add(titleBarStack);
            this.Children.Add(lineLabel);

        }

        public bool IsNavigateBack
        {
            get
            {
                return isClickBack;
            }
        }
        public Grid GridTitle
        {
            get
            {
                return this.gridTitle;
            }
        }

        public Label TitleLabel
        {
            get
            {
                return this.titleLabel;
            }
            set => this.titleLabel = TitleLabel;
        }

        public Label SubTitleLabel
        {
            get
            {
                return this.subTitleLabel;
            }
            set => this.subTitleLabel = SubTitleLabel;
        }

        public Image LeftImage
        {
            get
            {
                return this.leftImage;
            }
            set => this.leftImage = LeftImage;
        }

        public Image RightImage
        {
            get
            {
                return this.rightImage;
            }
            set => this.rightImage = RightImage;
        }
    }
}
