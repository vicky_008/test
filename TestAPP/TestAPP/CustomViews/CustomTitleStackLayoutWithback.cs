﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestAPP
{
    public class CustomTitleStackLayoutWithBack : StackLayout
    {
        Label titleLabel, lineLabel, subTitleLabel;
        Image closeImage, backImage;
        Grid gridTitle;
        bool isClickBack = false;

        public double titleHeight
        {
            get
            {
                return DeviceInfo.navigationBarHeight - 1;
            }
        }

        public CustomTitleStackLayoutWithBack(Page page, String title, bool IsModal = false,bool navToRoot = false)
        {
            NavigationPage.SetHasBackButton(page, false);
            NavigationPage.SetHasNavigationBar(page, false);
            this.BackgroundColor = Color.Transparent;
            this.VerticalOptions = LayoutOptions.StartAndExpand;
            this.Orientation = StackOrientation.Vertical;
            this.Spacing = 0;


            var statusBarStack = new StackLayout
            {
                HeightRequest = DeviceInfo.statusBarHeight,
                BackgroundColor = Color.Transparent
            };

            var titleBarStack = new StackLayout
            {
                Spacing = 0,
                Orientation = StackOrientation.Horizontal,
                HeightRequest = titleHeight,
                BackgroundColor = Color.Transparent
            };
            //if (subTitle != "")
            //{
            //    titleBarStack.HeightRequest = titleHeight + Helper.GetInstance().SizeHeight(10);
            //}
            var titleLabelStack = new StackLayout
            {
                Spacing = 0,
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            titleLabel = new Label
            {
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = title,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.Black,
                LineBreakMode = LineBreakMode.TailTruncation,
                WidthRequest = (DeviceInfo.deviceWidth / 10) * 7
            };
#if __ANDROID__
            titleLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
#endif
            subTitleLabel = new Label
            {
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start,
                Text = "",
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.Black,
                LineBreakMode = LineBreakMode.TailTruncation
            };

            titleLabelStack.Children.Add(titleLabel);
            //titleLabelStack.Children.Add(subTitleLabel);

            lineLabel = new Label
            {
                BackgroundColor = Color.Black,
                HeightRequest = 1

            };

            var closeStack = new StackLayout
            {
                Spacing = 0,
                Padding = Helper.GetInstance().CommonPadding(0, 10, 8, 0),
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
            };

            closeImage = new Image
            {
                HeightRequest = Helper.GetInstance().SizeWidth(22),
                WidthRequest = Helper.GetInstance().SizeWidth(22),
                HorizontalOptions = LayoutOptions.Start,
                Source = "close_button.png"
            };


            closeStack.Children.Add(closeImage);

            var closeTapGestureRecognizer = new TapGestureRecognizer();
            closeTapGestureRecognizer.Tapped += (s, e) =>
            {
                if (navToRoot)
                {
#if __IOS__

                    Navigation.PopToRootAsync(true);
#endif
#if __ANDROID__
                    //Pop To Root async not working in android, Meanwhile removing intermediate pages from Stack to go back,
                    //It wont remove current and Last and First page
                    for (int i = 1; i < Navigation.NavigationStack.Count; i++)
                    {
                        Navigation.RemovePage(Navigation.NavigationStack.ElementAt(i));

                    }
                    Navigation.PopModalAsync(true);
#endif
                }
                else
                {
                    Navigation.PopModalAsync(true);
                }
                Console.WriteLine("Clicked Back");
                isClickBack = true;
            };
            closeImage.GestureRecognizers.Add(closeTapGestureRecognizer);


            var backStack = new StackLayout
            {
                Spacing = 0,
                Padding = new Thickness(0, 0, 0, 0),
                VerticalOptions = LayoutOptions.Center
            };

            backImage = new Image
            {
                HeightRequest = Helper.GetInstance().SizeWidth(22),
                WidthRequest = Helper.GetInstance().SizeWidth(22),
                HorizontalOptions = LayoutOptions.Start,
                Source = "back_icon.png"
            };

            backStack.Children.Add(backImage);

            var backTapGestureRecognizer = new TapGestureRecognizer();
            backTapGestureRecognizer.Tapped += (s, e) =>
            {
                if (navToRoot)
                {
#if __IOS__

                    Navigation.PopToRootAsync(true);
#endif
#if __ANDROID__
                    //Pop To Root async not working in android, Meanwhile removing intermediate pages from Stack to go back,
                    //It wont remove current and Last and First page
                    for (int i = 1; i < Navigation.NavigationStack.Count; i++)
                    {
                        Navigation.RemovePage(Navigation.NavigationStack.ElementAt(i));

                    }
                    Navigation.PopAsync(true);
#endif
                }
                else
                {
                    page.Navigation.PopAsync(true);
                }
                Console.WriteLine("Clicked Back");
                isClickBack = true;
            };
            backImage.GestureRecognizers.Add(backTapGestureRecognizer);

            if (DeviceInfo.deviceFamily == "iPad")
            {
                closeImage.HeightRequest = 22;
                closeImage.WidthRequest = 22;
                closeStack.Padding = new Thickness(0, 10, 8, 0);
            }

            gridTitle = new Grid
            {
                BackgroundColor = Color.Transparent,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                WidthRequest = DeviceInfo.deviceWidth,

                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength((DeviceInfo.deviceWidth)  , GridUnitType.Absolute) }                    
                }
            };

            gridTitle.Children.Add(titleLabelStack, 0, 0);
            if (!IsModal)
                gridTitle.Children.Add(backStack, 0, 0);
            else
                gridTitle.Children.Add(closeStack, 0, 0);

            titleBarStack.Children.Add(gridTitle);

            this.Children.Add(statusBarStack);
            this.Children.Add(titleBarStack);
            this.Children.Add(lineLabel);

        }

        public bool IsNavigateBack
        {
            get
            {
                return isClickBack;
            }
        }
        public Grid GridTitle
        {
            get
            {
                return this.gridTitle;
            }
        }

        public Label TitleLabel
        {
            get
            {
                return this.titleLabel;
            }
            set => this.titleLabel.Text = TitleLabel.Text;
        }

        public Label SubTitleLabel
        {
            get
            {
                return this.subTitleLabel;
            }
            set => this.subTitleLabel.Text = SubTitleLabel.Text;
        }
    }
}
