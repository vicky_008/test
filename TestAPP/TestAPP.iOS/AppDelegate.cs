﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace TestAPP.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public static double deviceWidth { get; set; }
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            InitializeSizes();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

        public string Version
        {
            get
            {
                NSObject ver = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
                return ver.ToString();
            }
        }

        private void InitializeSizes()
        {
            try
            {
                //Platoform specific for device aspect
                DeviceInfo.deviceHeight = UIScreen.MainScreen.Bounds.Height;
                DeviceInfo.deviceWidth = UIScreen.MainScreen.Bounds.Width;
                deviceWidth = UIScreen.MainScreen.Bounds.Width;

                if (DeviceInfo.deviceHeight < DeviceInfo.deviceWidth)
                {
                    DeviceInfo.deviceHeight = UIScreen.MainScreen.Bounds.Width;
                    DeviceInfo.deviceWidth = UIScreen.MainScreen.Bounds.Height;
                }

                DeviceInfo.statusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Height;
                var nav = new UINavigationController();
                DeviceInfo.navigationBarHeight = nav.NavigationBar.Frame.Height;

                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    DeviceInfo.deviceFamily = "iPhone";
                else
                    DeviceInfo.deviceFamily = "iPad";

                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone && UIScreen.MainScreen.NativeBounds.Height == 2436)
                    DeviceInfo.deviceName = "iPhoneX";

                DeviceInfo.currentVersion = Version;

            }
            catch (Exception ex)
            {
                GASCall.Track_App_Exception(ex.ToString(), false);
            }

        }
    }
}
