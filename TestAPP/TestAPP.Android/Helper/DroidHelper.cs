﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TestAPP.Droid
 {
    class DroidHelper
    {
        private static DroidHelper _instance = null;
        public static DroidHelper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DroidHelper();
            }
            return _instance;
        }
        public void SetDefaultHeightAndWidth()
        {
            try
            {
                var activity = Xamarin.Forms.Forms.Context as MainActivity;
                var metrics = activity.Resources.DisplayMetrics;
                var widthInDp = ConvertPixelsToDp(activity, metrics.WidthPixels);
                var heightInDp = ConvertPixelsToDp(activity, metrics.HeightPixels);
                DeviceInfo.deviceWidth = widthInDp;
                DeviceInfo.deviceHeight = heightInDp;
                //DeviceInfo.deviceWidth = (widthInDp - 0.5f) / metrics.Density;
                //DeviceInfo.deviceHeight = (heightInDp - 0.5f) / metrics.Density;
                DeviceInfo.statusBarHeight = 0;
                DeviceInfo.navigationBarHeight = 50;
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                {
                    activity.Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
                    activity.Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
                }
                DeviceInfo.currentVersion = Version;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        int ConvertPixelsToDp(Activity activity, float pixelValue)
        {
            var dp = (int)((pixelValue) / activity.Resources.DisplayMetrics.Density);
            return dp;
        }
        string Version
        {
            get
            {
                var info = Application.Context.ApplicationContext.PackageManager.GetPackageInfo(Application.Context.ApplicationContext.PackageName, 0).VersionName;

                return info;
            }
        }
    }
}